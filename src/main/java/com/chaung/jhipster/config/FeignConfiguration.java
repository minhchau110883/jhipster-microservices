package com.chaung.jhipster.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.chaung.jhipster")
public class FeignConfiguration {

}
