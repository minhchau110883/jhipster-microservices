package com.chaung.jhipster.cucumber.stepdefs;

import com.chaung.jhipster.JhipstermicroservicesApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = JhipstermicroservicesApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
